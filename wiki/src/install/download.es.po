# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2022-02-25 15:40-0600\n"
"PO-Revision-Date: 2022-12-21 11:07+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"install-download/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Download and verify (for USB sticks)\"]]\n"
msgstr "[[!meta title=\"Descargar y verificar (para memorias USB)\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/download\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"install/inc/stylesheets/download\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"doc/about/warnings\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"doc/about/warnings\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"install/inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"install/inc/stylesheets/download-only-img\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"install/inc/stylesheets/download-only-img\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta script=\"install/inc/js/download\"]]\n"
msgstr "[[!meta script=\"install/inc/js/download\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta script=\"install/inc/js/forge.sha256.min\"]]\n"
msgstr "[[!meta script=\"install/inc/js/forge.sha256.min\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/download.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/download.inline.es\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/verify.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""
"[[!inline pages=\"install/inc/steps/verify.inline.es\" raw=\"yes\" sort=\"age"
"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "<h1 class=\"step\">Continue installing or upgrading Tails</h1>\n"
msgstr "<h1 class=\"step\">Continuar instalando o actualizando Tails</h1>\n"

#. type: Plain text
msgid "With this USB image, you can either:"
msgstr "Con esta imagen USB, puede:"

#. type: Plain text
msgid "- Upgrade your Tails USB stick and keep your Persistent Storage:"
msgstr ""
"- Actualizar tu memoria USB de Tails y mantener tu Almacenamiento "
"Persistente:"

#. type: Bullet: '  * '
msgid "[[Upgrade from your Tails|upgrade/tails]]"
msgstr "[[Actualizar desde tu Tails|upgrade/tails]]"

#. type: Bullet: '  * '
msgid "[[Upgrade from Windows|upgrade/windows]]"
msgstr "[[Actualizar desde Windows|upgrade/windows]]"

#. type: Bullet: '  * '
msgid "[[Upgrade from macOS|upgrade/mac]]"
msgstr "[[Actualizar desde macOS|upgrade/mac]]"

#. type: Bullet: '  * '
msgid "[[Upgrade from Linux|upgrade/linux]]"
msgstr "[[Actualizar desde Linux|upgrade/linux]]"

#. type: Plain text
msgid "- Install a new Tails USB stick:"
msgstr "- Instalar una nueva memoria USB de Tails:"

#. type: Bullet: '  * '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Instalar desde Windows|install/windows]]"

#. type: Bullet: '  * '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Instalar desde macOS|install/mac]]"

#. type: Bullet: '  * '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Instalar desde Linux|install/linux]]"

#. type: Bullet: '  * '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Instalar desde Debian o Ubuntu usando la linea de comandos y GnuPG|install/"
"expert]]"

#. type: Plain text
msgid ""
"To use Tails in a virtual machine or burn a Tails DVD, download our [[ISO "
"image|download-iso]] instead."
msgstr ""
"Para usar Tails en una máquina virtual o grabar un DVD de Tails, descarga "
"nuestra [[imagen ISO|download-iso]]."

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/warnings.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""
"[[!inline pages=\"install/inc/steps/warnings.inline.es\" raw=\"yes\" sort="
"\"age\"]]\n"

#, no-wrap
#~ msgid "[[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
#~ msgstr "[[!meta stylesheet=\"install/inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"

#~ msgid "[[!meta robots=\"noindex\"]]\n"
#~ msgstr "[[!meta robots=\"noindex\"]]\n"
