# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-10-31 09:58+0100\n"
"PO-Revision-Date: 2023-11-02 23:11+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Title ###
#, no-wrap
msgid "Connecting to the Internet and Tor"
msgstr "Connexió a Internet i Tor"

#. type: Bullet: '- '
msgid "[[Connecting to a local network|anonymous_internet/networkmanager]]"
msgstr "[[Connexió a una xarxa local|anonymous_internet/networkmanager]]"

#. type: Bullet: '  - '
msgid "[[Troubleshooting Wi-Fi not working|anonymous_internet/no-wifi]]"
msgstr ""
"[[Resolució de problemes quan el Wi-Fi no funciona|anonymous_internet/no-"
"wifi]]"

#. type: Bullet: '  - '
msgid ""
"[[Signing in to a network using a captive portal|anonymous_internet/"
"unsafe_browser]]"
msgstr ""
"[[Iniciar sessió a una xarxa que usa un portal captiu|anonymous_internet/"
"unsafe_browser]]"

#. type: Bullet: '- '
msgid "[[Connecting to the Tor network|anonymous_internet/tor]]"
msgstr "[[Connexió a la xarxa Tor|anonymous_internet/tor]]"

#. type: Bullet: '  - '
msgid ""
"[[Troubleshooting connecting to Tor|anonymous_internet/tor/troubleshoot]]"
msgstr ""
"[[Resolució de problemes de connexió a Tor|anonymous_internet/tor/"
"troubleshoot]]"

#. type: Bullet: '  - '
msgid "[[Why does Tails use Tor?|anonymous_internet/tor/why]]"
msgstr "[[Per què Tails utilitza Tor?|anonymous_internet/tor/why]]"

#. type: Bullet: '  - '
msgid "[[Why is Tor slow?|anonymous_internet/tor/slow]]"
msgstr "[[Per què Tor és lent?|anonymous_internet/tor/slow]]"

#. type: Bullet: '  - '
msgid "[[Managing the circuits of Tor|anonymous_internet/tor/circuits]]"
msgstr "[[Gestió dels circuits de Tor|anonymous_internet/tor/circuits]]"

#. type: Title ###
#, no-wrap
msgid "Internet applications"
msgstr "Aplicacions d'Internet"

#. type: Plain text
msgid ""
"- [[Browsing the web with *Tor Browser*|anonymous_internet/tor_browser]] - "
"[[Sharing files with *OnionShare*|anonymous_internet/onionshare]] - "
"[[Emailing and reading news with *Thunderbird*|anonymous_internet/"
"thunderbird]] - [[Exchanging bitcoins using *Electrum*|anonymous_internet/"
"electrum]] - [[Chatting with *Pidgin* and OTR|anonymous_internet/pidgin]]"
msgstr ""
"- [[Navegar pel web amb el *Navegador Tor*|anonymous_internet/tor_browser]]\n"
"- [[Compartir fitxers amb *OnionShare*|anonymous_internet/onionshare]]\n"
"- [[Enviar correus electrònics i llegir notícies amb *Thunderbird*|"
"anonymous_internet/thunderbird]]\n"
"- [[Intercanviar bitcoins mitjançant *Electrum*|anonymous_internet/"
"electrum]]\n"
"- [[Xatejar amb *Pidgin* i OTR|anonymous_internet/pidgin]]"

#~ msgid "[[Viewing the circuits of Tor|anonymous_internet/tor/circuits]]"
#~ msgstr "[[Veure els circuits de Tor|anonymous_internet/tor/circuits]]"
