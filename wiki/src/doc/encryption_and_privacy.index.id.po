# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2023-01-27 18:26+0100\n"
"PO-Revision-Date: 2016-02-06 16:39+0100\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: \n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"

#. type: Bullet: '  - '
#, fuzzy
msgid ""
"[[Managing passwords using *KeePassXC*|encryption_and_privacy/"
"manage_passwords]]"
msgstr ""
"[[!traillink Erstellen_und_Benutzen_verschlüsselter_Laufwerke|"
"encryption_and_privacy/encrypted_volumes]]"

#. type: Bullet: '  - '
#, fuzzy
msgid ""
"[[Creating and using *LUKS* encrypted volumes|encryption_and_privacy/"
"encrypted_volumes]]"
msgstr ""
"[[!traillink Erstellen_und_Benutzen_verschlüsselter_Laufwerke|"
"encryption_and_privacy/encrypted_volumes]]"

#. type: Bullet: '  - '
#, fuzzy
msgid ""
"[[Using *VeraCrypt* encrypted volumes|encryption_and_privacy/veracrypt]]"
msgstr ""
"[[!traillink Erstellen_und_Benutzen_verschlüsselter_Laufwerke|"
"encryption_and_privacy/encrypted_volumes]]"

#. type: Bullet: '  - '
#, fuzzy
msgid ""
"[[Encrypting text and files using *GnuPG* and *Kleopatra*|"
"encryption_and_privacy/kleopatra]]"
msgstr ""
"[[!traillink Erstellen_und_Benutzen_verschlüsselter_Laufwerke|"
"encryption_and_privacy/encrypted_volumes]]"

#. type: Bullet: '  - '
msgid ""
"[[Securely deleting files and clean diskspace|encryption_and_privacy/"
"secure_deletion]]"
msgstr ""

#. type: Bullet: '  - '
#, fuzzy
msgid ""
"[[Calculating checksums using *GtkHash*|encryption_and_privacy/checksums]]"
msgstr ""
"[[!traillink Erstellen_und_Benutzen_verschlüsselter_Laufwerke|"
"encryption_and_privacy/encrypted_volumes]]"

#. type: Bullet: '  - '
#, fuzzy
msgid "[[Using the screen keyboard|encryption_and_privacy/virtual_keyboard]]"
msgstr ""
"[[!traillink Erstellen_und_Benutzen_verschlüsselter_Laufwerke|"
"encryption_and_privacy/encrypted_volumes]]"
