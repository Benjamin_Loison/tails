# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-03-09 02:52+0000\n"
"PO-Revision-Date: 2023-11-12 22:12+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Results of the 2020 donation campaign\"]]\n"
msgstr "[[!meta title=\"Resultats de la campanya de donacions 2020\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 09 Mar 2021 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 09 Mar 2021 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"Thank you so much, on behalf of our entire team, to everyone who donated "
"during our end-of-year campaign!"
msgstr ""
"Moltes gràcies, en nom de tot el nostre equip, a tots els que heu fet "
"donacions durant la nostra campanya de final d'any!"

#. type: Plain text
msgid ""
"We raised $132&#8239;750 from 2&#8239;799 donors. That's an amazing 40% more "
"than last year and 30% of our yearly budget. These excellent numbers mean "
"more sustainability for Tails and ultimately better tools for you and all "
"our users."
msgstr ""
"Hem recaptat 132&#8239;750 de 2&#8239;799 donants. Això suposa un 40% més "
"que l'any passat i un 30% del nostre pressupost anual. Aquests excel·lents "
"números signifiquen més sostenibilitat per a Tails i, en definitiva, millors "
"eines per a tots els nostres usuaris."

#. type: Plain text
msgid "Here is what we did since October with your help:"
msgstr "Aquí teniu el que hem fet des d'octubre amb la vostra ajuda:"

#. type: Bullet: '- '
msgid ""
"From the number of automatic upgrades, we know that Tails was used 20% more "
"in 2020 than in 2019. It's our biggest increase since 2016."
msgstr ""
"Pel nombre d'actualitzacions automàtiques, sabem que Tails s'ha utilitzat un "
"20% més el 2020 que el 2019. És el nostre augment més gran des del 2016."

#. type: Bullet: '- '
msgid ""
"**We designed and tested usability improvements to the Persistent Storage "
"and the connection to Tor.**"
msgstr ""
"**Hem dissenyat i provat millores d'usabilitat a l'Emmagatzematge Persistent "
"i a la connexió a Tor.**"

#. type: Plain text
#, no-wrap
msgid ""
"  We know from previous surveys and usability research that these are 2\n"
"  critical aspects to improve the usability of Tails, especially for\n"
"  less technical users and people experiencing heavy online censorship,\n"
"  for example in Asia.\n"
msgstr ""
"  Sabem per enquestes anteriors i investigacions d'usabilitat que aquests "
"són dos\n"
"  aspectes crítics per millorar la usabilitat de Tails, especialment per a\n"
"  usuaris menys tècnics i persones que pateixen una forta censura en línia,\n"
"  per exemple a Àsia.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  Your donations will help us implement as many of these improvements as\n"
"  possible during 2021. You can already have a look at the new designs\n"
"  in our blueprints on the [[!tails_blueprint\n"
"  Persistent-Storage-redesign desc=\"Persistent Storage\"]] and the\n"
"  [[!tails_blueprint network_connection desc=\"connection to Tor\"]].\n"
msgstr ""
"  Les vostres donacions ens ajudaran a implementar tantes d'aquestes "
"millores com\n"
"  sigui possible durant el 2021. Ja podeu fer una ullada als nous dissenys\n"
"  dels nostres plànols de l'[[!tails_blueprint\n"
"  Persistent-Storage-redesign desc=\"Emmagatzematge Persistent\"]] i la\n"
"  [[!tails_blueprint network_connection desc=\"connexió a Tor\"]].\n"

#. type: Plain text
#, no-wrap
msgid ""
"  We always test the usability of major features with users before\n"
"  releasing them. It was particularly exciting this time because we\n"
"  tested our design remotely using <a\n"
"  href=\"https://simplysecure.org/blog/formative-testing\">paper\n"
"  prototyping</a> with people in Greece, Mexico, Italy, Uganda, Germany,\n"
"  Ireland, and Malaysia!\n"
msgstr ""
"  Abans de llançar els canvis sempre provem la usabilitat de les principals "
"funcions\n"
"  amb els usuaris. Aquesta vegada va ser especialment emocionant perquè "
"nosaltres\n"
"  vam provar el nostre disseny de forma remota utilitzant <a href=\"https"
"://simplysecure.org/blog/formative-testing\">paper\n"
"  prototipat</a> amb persones de Grècia, Mèxic, Itàlia, Uganda, Alemanya,\n"
"  Irlanda i Malàisia!\n"

#. type: Bullet: '- '
msgid ""
"**We simplified the verification procedure on our download page** by "
"[[replacing the *Tails Verification* extension|"
"verification_extension_deprecation]] with similar JavaScript code that runs "
"[[directly on the download page|install/download]]."
msgstr ""
"**Hem simplificat el procediment de verificació a la nostra pàgina de "
"baixada** [[substituint l'extensió *Tails "
"Verification*|verification_extension_deprecation]] per un codi de JavaScript "
"similar que s'executa [[directament a la pàgina de baixada|install/"
"download]]."

#. type: Plain text
#, no-wrap
msgid ""
"  This simple usability improvement had a huge impact and 60% more\n"
"  downloads are verified and secure now than with the verification\n"
"  extension.\n"
msgstr ""
"  Aquesta senzilla millora de la usabilitat va tenir un impacte enorme i un "
"60% més\n"
"  de les baixades estan verificades i segures ara que amb la verificació\n"
"  feta per l'extensió.\n"

#. type: Plain text
msgid ""
"- **We automated the most time-consuming parts of our release process.**"
msgstr ""
"- **Hem automatitzat les parts que més temps consumeixen del nostre procés "
"de llançament.**"

#. type: Plain text
#, no-wrap
msgid ""
"  We estimate that we divided roughly by 2 the amount of time, work, and\n"
"  stress needed for us to publish a new version of Tails.\n"
msgstr ""
"  Calculem que hem dividit aproximadament a la meitat la quantitat de temps, "
"treball i\n"
"  l'estrès necessari perquè puguem publicar una nova versió de Tails.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  For you, this means a quicker response time in case of important\n"
"  security vulnerability and more time available for us to work on new\n"
"  features and improvements.\n"
msgstr ""
"  Per als usuaris, això significa un temps de resposta més ràpid en cas "
"d'una important\n"
"  vulnerabilitat de seguretat i més temps disponible per treballar en les "
"noves\n"
"  característiques i millores.\n"

#. type: Plain text
msgid "- **We hired a new core developer, boyska.**"
msgstr "- **Hem contractat un nou desenvolupador bàsic, Boyska.**"

#. type: Plain text
#, no-wrap
msgid ""
"  boyska has very strong experience in the technical aspects of\n"
"  operating systems and software development, and also in the social\n"
"  implications of technology, Free Software, and privacy.\n"
msgstr ""
"  boyska té una gran experiència en els aspectes tècnics de\n"
"  desenvolupament de sistemes operatius i programari, i també en les "
"implicacions\n"
"  socials de la tecnologia, el Programari Lliure i la privadesa.\n"

#. type: Plain text
#, no-wrap
msgid ""
"  boyska will be key to bringing more improvements to Tails more\n"
"  quickly.\n"
msgstr ""
"  boyska serà clau per aportar millores a Tails més\n"
"  ràpidament.\n"

#. type: Plain text
msgid "Thanks again for supporting Tails!"
msgstr "Gràcies de nou per donar suport a Tails!"
